#include "threads.h"
#include <iostream>
#include <thread>
#include <ctime>

bool isPrime(int n)
{
	// Corner case 
	if (n <= 1)
		return false;

	// Check from 2 to n-1 
	for (int i = 2; i < n; i++)
		if (n % i == 0)
			return false;

	return true;
}

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads); // at this point thread t1 is running
	t1.join(); // wait until t1 will end
}

void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++) {
		std::cout << primes[i] << std::endl;
	}
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	for (int i = begin; i <= end; i++) {
		if (isPrime(i)) {
			primes.push_back(i);
		}
	}
}

std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	std::clock_t startTime = clock();
	std::thread t2(getPrimes, begin, end, std::ref(primes)); // at this point thread t2 is running
	t2.join(); // wait until t2 will end
	std::clock_t endTime = clock();
	double time = (endTime - startTime) / (double)CLOCKS_PER_SEC;;
	std::cout << "Thread's time (Seconds): " << time << std::endl;
	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{ 
	if (file.is_open()){
		std::cout << "Able to open file" << std::endl;
		for (int i = begin; i <= end; i++) {
			if (isPrime(i)) {
				file << i << "\n";
			}
		}
	}
	else {
		std::cout << "Unable to open file" << std::endl;
	}


}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file;
	int amount;
	amount = (end - begin + 1) / N;
	int start = begin;
	std::clock_t startTime = clock();
	file.open(filePath);
	for (int i = 0; i < N; i++) {
		std::thread t(writePrimesToFile, start, start+amount, std::ref(file));
		t.join();
		start += amount;
	}
	file.close();
	std::clock_t endTime = clock();
	double time = (endTime - startTime) / (double)CLOCKS_PER_SEC;;
	std::cout << "Thread's time (Seconds): " << time << std::endl;


}
